# Spacecast

A P2P podcast player where shows are hosted directly by the audience. Data is stored in the browser and shared via P2P data services. 

Built with Ethereum and IPFS.